package org.tringapps.constants;


public class SqlQueries {
    public static final String employeeWeeklyAttendanceRecords = "select e from EmployeeAttendance e where date =: ";
    public static final String getAllEmployeeIds = "select employeeId from Employee ";
    public static final String getAllEmployeeLeaveCount = "select e.employee, count(e.date) from EmployeeLeaveDetails e where e.date between" +
            " :startDate and :endDate group by e.employee";
    public static final String getLeaveCount = "select ld.count from leave_details ld where ld.type =:leaveType";
    public static final String getCasualLeaveCount = "Select e.employee, e.casualLeaveCount from EmployeeLeaveCount e";
    public static final String getEmployeeLeaves = "select e from EmployeeLeaveDetails e where e.employee =:employee";
}
