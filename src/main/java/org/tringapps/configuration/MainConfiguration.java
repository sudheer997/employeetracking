package org.tringapps.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SuppressWarnings("deprecation")
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"org.tringapps"})
public class MainConfiguration extends WebMvcConfigurerAdapter {

	@Bean(name = "multipartResolver")
    public CommonsMultipartResolver getMultiPart(){
        return new CommonsMultipartResolver();
    }

}
