/*
 * package org.tringapps.servlets;
 * 
 * import org.springframework.context.ApplicationContext; import
 * org.springframework.context.annotation.AnnotationConfigApplicationContext;
 * import org.tringapps.configuration.MainConfiguration; import
 * org.tringapps.services.EmployeeAttendanceService;
 * 
 * import javax.servlet.ServletException; import
 * javax.servlet.annotation.WebServlet; import javax.servlet.http.HttpServlet;
 * import javax.servlet.http.HttpServletRequest; import
 * javax.servlet.http.HttpServletResponse; import java.io.IOException; import
 * java.io.PrintWriter; import java.text.ParseException; import java.util.Map;
 * 
 * @WebServlet("") public class EmployeeAtt extends HttpServlet { private static
 * final long serialVersionUID = 1L;
 * 
 * protected void doGet(HttpServletRequest request, HttpServletResponse
 * response) { ApplicationContext context = new
 * AnnotationConfigApplicationContext(MainConfiguration.class);
 * EmployeeAttendanceService employeeAttendanceService =
 * context.getBean(EmployeeAttendanceService.class); try { Map<Integer, Long>
 * totalWorkingHours = employeeAttendanceService.getWorkingHours(); PrintWriter
 * writer = response.getWriter(); writer.print(totalWorkingHours);
 * 
 * } catch (ParseException | IOException e) { e.printStackTrace(); }
 * 
 * } }
 * 
 */