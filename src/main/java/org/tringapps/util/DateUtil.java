package org.tringapps.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DateUtil {
    public static String format = "dd/MM/yyyy";

    public static long timeToMilliSecs(String time) throws ParseException {
        boolean isFormat = checkTimeFormat(time);
        if(!isFormat){
            return -1;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = sdf.parse("1970-01-01 " + time);
        System.out.println("in milliseconds: " + date.getTime());
        return date.getTime();
    }

    public static String milliSecondsToTime(long milliSeconds){
        int parsedMillSeconds = (int) ((milliSeconds /1000) /60);
        int hours = (int) (parsedMillSeconds /60);
        int minutes = (int) ((parsedMillSeconds)%60);
        return String.format("hours : %d : minutes : %d", hours, minutes);
    }

    public static boolean checkTimeFormat(String time){
        if(time == null){
            return false;
        }
        String hoursRegEx = "^(\\d|[01]\\d|2[0-4])";
        String minutesRegEx = "^(\\d|[01]\\d|2\\d|3\\d|4\\d|5\\d)";
        String[] items =time.split(":");
        return items.length == 2 && items[0].trim().matches(hoursRegEx) && items[1].trim().matches(minutesRegEx);
    }

    public static long dateToTimeStamp(String value, String format) {
        Date date = null;
        if(format == null){
            format = "dd/MM/yyyy";
        }
        value = checkDateFormat(value);
        if(value == null){
            return -1;
        }
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            date = sdf.parse(value);
            System.out.println(date);
            if (!value.trim().equals(sdf.format(date))) {
                System.out.println("Invalid date format");
                return -1;
            }
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static String timeStampToDate(long value, String format){
        if(format == null){
            format = "dd/MM/yyyy";
        }
        Timestamp ts = new Timestamp(value);
        Date date = new Date(ts.getTime());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

    public static String checkDateFormat(String date){
        if(date == null){
            return null;
        }
        String[] items = date.split("/", 0);
        if(items.length != 3){
            return null;
        }
        StringBuilder formatDate = new StringBuilder();
        for(int i=0; i<items.length; i++){
            if(items[i].length() == 1){
                items[i] = "0" + items[i];
            }
            formatDate.append(items[i]);
            if(i != items.length-1){
                formatDate.append("/");
            }
        }
        System.out.println(formatDate);
        return formatDate.toString();
    }

    public static String getCurrentDate(){
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

    public static int  getCurrentDay(){
        Date date = new Date();
        return date.getDay();
    }

    public static int totalWorkingDays(long startDate, long endDate){
        int workingDays =0;
        Calendar start_date = Calendar.getInstance();
        start_date.setTime(new Date(startDate));
        Calendar end_date = Calendar.getInstance();
        end_date.setTime(new Date(endDate));
        end_date.add(Calendar.DATE, -1);
        while (! start_date.after(end_date)){
            int day = start_date.get(Calendar.DAY_OF_WEEK);
            if(day != Calendar.SATURDAY && day != Calendar.SUNDAY){
                workingDays++;
            }
            start_date.add(Calendar.DATE, 1);
        }
        System.out.println(workingDays);
        return workingDays;
    }

    public static Long[] getMonthStartAndEndDate(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String end_date = "01/07/2020";
        Calendar calendar = Calendar.getInstance();
        try {
            Date end = simpleDateFormat.parse(end_date);
            System.out.println("End date : " + end);
            long endDate = end.getTime();
            calendar.setTime(end);
            calendar.add(Calendar.MONTH, -1);
            System.out.println(calendar.getTime().getMonth());
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            System.out.println( "sdsds" +calendar.getTime());
            return new Long[]{calendar.getTime().getTime(), endDate};
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    public static Long[] getStartDateAndEndDate() {
        /*LocalDate endDate = LocalDate.now();
        if(endDate.getDayOfWeek().getValue() == 5){
            return null;
        }*/
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String end_date = "05/06/2020";
        Calendar calendar = Calendar.getInstance();
        try {
            Date end = simpleDateFormat.parse(end_date);
            long endDate = end.getTime();
            calendar.setTime(end);
            calendar.set(Calendar.DAY_OF_WEEK, 2);
            if(end.getMonth() == calendar.getTime().getMonth()){
                return new Long[]{calendar.getTime().getTime(), endDate};
            }
            calendar.setTime(end);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            return new Long[]{calendar.getTime().getTime(), endDate};
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

        /*LocalDate endDate = LocalDate.parse("10/06/2020", DateTimeFormatter.ofPattern(format));
        long end_date = simpleDateFormat.parse(endDate.toString()).getTime();
        System.out.println(timeStampToDate(end_date, "dd/MM/yyyy"));
        LocalDate startDate = endDate.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        System.out.println(startDate);
        System.out.println("day of week" + startDate.getDayOfWeek().getValue());
        if(startDate.getMonth() == endDate.getMonth() ){
            System.out.println("working");
            long start_date = simpleDateFormat.parse(startDate.toString()).getTime();
            System.out.println(timeStampToDate(start_date, "dd/MM/yyyy"));
            return new Long[] { start_date, end_date};
        }
        Calendar calendar = Calendar.getInstance();
        System.out.println(calendar.getFirstDayOfWeek());
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        long start_date = calendar.getTime().getTime();
        System.out.println(timeStampToDate(start_date, "dd/MM/yyyy"));
        return new Long[] { start_date,  end_date};*/

        /*System.out.println("\nNext Friday: "+dt.with(TemporalAdjusters.next(DayOfWeek.FRIDAY)));
        System.out.println("Previous Friday: "+dt.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY))+"\n");*/
        /*Date endDate = new Date();
        if(endDate.getDay() != 5){
            return;
        }
        Calendar startDate = Calendar.getInstance();
        startDate.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        if(startDate.getTime().getMonth() == endDate.getMonth()){
            return;
        }
        Calendar calendar = Calendar.getInstance();
        System.out.println(calendar.getFirstDayOfWeek());
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));*/

    }

    public static List<Long> getListDays(long startDate, long endDate){
        long interval = 24*1000 * 60 * 60;
        long curTime = startDate;

        List<Long> dates = new ArrayList<>();
        while (curTime <= endDate){
            dates.add(new Date(curTime).getTime());
            curTime += interval;
        }
        return dates;
    }

    public static void getStartingDayOfWeek(){
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
        System.out.println(c.getTime().getMonth());
    }

    public static int getPreviousMonth(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        System.out.println(calendar.getTime());
        return calendar.getTime().getMonth() + 1;
    }

    public static int getPreviousMonthYear(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 0);
        return calendar.getTime().getYear() + 1900;
    }

    public static Long getActualHours(int workingDays){
        return (long) (workingDays * 10*1000 * 60 * 60);
    }

    public static void main(String[] args) throws ParseException {
        System.out.println(getPreviousMonth());
        System.out.println(getPreviousMonthYear());
        Long[] start = getMonthStartAndEndDate();
        totalWorkingDays(start[0], start[1]);
        /*Long[] sd = getStartDateAndEndDate();
        System.out.println(timeStampToDate(sd[0], "dd/MM/yyyy"));
        System.out.println(timeStampToDate(sd[1], "dd/MM/yyyy"));
        System.out.println(Arrays.toString(getStartDateAndEndDate()));*/
    }

    /*public static void main(String[] args) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        LocalDate endDate = LocalDate.parse("16/02/2020", DateTimeFormatter.ofPattern(format));
        LocalDate startDate = LocalDate.parse("12/02/2020", DateTimeFormatter.ofPattern(format));
        long numOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate);
        List<LocalDate> dates = IntStream.iterate(0, i -> i + 1)
                .limit(numOfDaysBetween)
                .mapToObj(startDate::plusDays)
                .collect(Collectors.toList());
        System.out.println(dates.toString());
//        System.out.println(getCurrentDay());
        getStartDateAndEndDate();
        *//*long checkInTime = timeToMilliSecs("24:00");
        System.out.println(checkInTime);
        System.out.println(milliSecondsToTime(checkInTime));
        long checkOutTime = timeToMilliSecs("19:30");
        System.out.println(milliSecondsToTime(checkOutTime - checkInTime));*//*
    }*/

    /*public static void main(String[] args){
        long timeStamp = dateToTimeStamp("1/2/2020", "dd/MM/yyyy");
        System.out.println(timeStamp);
        String date = timeStampToDate(timeStamp, "dd/MM/yyyy");
        System.out.println(date);
    }*/
}
