package org.tringapps.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.tringapps.dao.EmployeeAttendanceDao;
import org.tringapps.entities.Employee;
import org.tringapps.entities.EmployeeAttendance;
import org.tringapps.util.HibernateUtil;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Repository
public class EmployeeAttendanceDaoImpl implements EmployeeAttendanceDao {

    public void persistMultipleRecords(Map<Integer, List<EmployeeAttendance>> employeeAttendanceRecords){
        Transaction trx = null;
        if(employeeAttendanceRecords == null){
            return;
        }
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            trx = session.beginTransaction();
            for(Map.Entry<Integer, List<EmployeeAttendance>> employeeAttendance : employeeAttendanceRecords.entrySet()){
                for(EmployeeAttendance employeeAttendance1 : employeeAttendance.getValue()){
                    session.save(employeeAttendance1);
                }
                session.flush();
                session.clear();
            }
            session.getTransaction().commit();
        } catch (HibernateException e) {
            if (trx != null) {
                trx.rollback();
            }
            e.printStackTrace();
        }
    }

    public int persistRecord(EmployeeAttendance employeeAttendance) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction trx = null;
        int id = 0;
        try {
            trx = session.beginTransaction();
            id = (Integer) session.save(employeeAttendance);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            if (trx != null) {
                trx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return id;
    }

    public int updateCheckOutTime(int checkOutTime, int employeeId, long date) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        int id = 0;
        try {
            transaction = session.beginTransaction();
            Query updateQuery = session.createQuery("update EmployeeAttendance set checkOutTime =:checkOutTime " +
                    "where employeeId =:id and date =: date");
            updateQuery.setParameter("checkOutTime", checkOutTime);
            updateQuery.setParameter("employeeId", employeeId);
            updateQuery.setParameter("date", date);
            id = updateQuery.executeUpdate();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return id;
    }

    public int deleteRecord(int employeeId, long date) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        int id = 0;
        try {
            transaction = session.beginTransaction();
            Query updateQuery = session.createQuery("update EmployeeAttendance set isDeleted =:isDeleted " +
                    "where employeeId =:id and date =: date");
            updateQuery.setParameter("isDeleted", true);
            updateQuery.setParameter("employeeId", employeeId);
            updateQuery.setParameter("date", date);
            id = updateQuery.executeUpdate();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return id;
    }

    public long getCheckInTime(int employeeId, long date) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        long checkInTime = 0;
        try {
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(EmployeeAttendance.class);
            Criterion rest1 = Restrictions.and(Restrictions.eq("employee_id", employeeId),
                    Restrictions.eq("date", date));
            EmployeeAttendance employeeAttendance = (EmployeeAttendance) criteria.add(rest1);
            checkInTime = employeeAttendance.getCheckInTime();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return checkInTime;
    }

    public List<EmployeeAttendance> getEmployeeWeeklyAttendanceRecords(long startDate, long endDate) {
        if (startDate == -1 && endDate == -1) {
            return null;
        }
        Transaction trx = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            trx = session.beginTransaction();
            Query updateQuery = session.createQuery("select e from EmployeeAttendance e where " +
                    "e.date between :startDate and :endDate");
            updateQuery.setParameter("startDate", startDate);
            updateQuery.setParameter("endDate", endDate);

            /*Criteria employeeAttendance = session.createCriteria(EmployeeAttendance.class);
            employeeAttendance.add(Restrictions.between("date", startDate, endDate));
            ProjectionList projectionList = Projections.projectionList();
            projectionList.add(Projections.groupProperty("state"));
            projectionList.add(Projections.rowCount());
            employeeAttendance.setProjection(projectionList);*/
            System.out.println(startDate +"dfg" + endDate);
            System.out.println(updateQuery.list());
            List<EmployeeAttendance> employeeAttendanceRecords = updateQuery.list();
            session.getTransaction().commit();
            return employeeAttendanceRecords;
        } catch (HibernateException e) {
            e.printStackTrace();
            if (trx != null) {
                trx.rollback();
            }
            return null;
        }
    }

    public Map<Integer, Long> getTotalWorkingHours(long startDate, long endDate){
        if (startDate == -1 && endDate == -1) {
            return null;
        }
        Transaction trx = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            trx = session.beginTransaction();
            Query updateQuery = session.createQuery("select e.employee, sum(e.totalHours) as total from EmployeeAttendance e where " +
                    "e.date between :startDate and :endDate group by e.employee");
            updateQuery.setParameter("startDate", startDate);
            updateQuery.setParameter("endDate", endDate);
            Map<Integer, Long> totalWorkingHours = new HashMap<>();
            for(Iterator it = updateQuery.iterate(); it.hasNext();)
            {
                Object[] row = (Object[]) it.next();
                Employee employee = (Employee) row[0];
                long totalHours = (long) row[1];
                totalWorkingHours.put(employee.getEmployeeId(), totalHours);
            }
            /*List<List<Long>> list = (List<List<Long>>) updateQuery.list();
			List<List<Long>> WorkingHours = list;

            System.out.println(WorkingHours);
            for(List<Long> record : WorkingHours){
                long id = record.get(0);
                int employeeId = (int) id;
                totalWorkingHours.put(employeeId, record.get(1));
            }*/
            session.getTransaction().commit();
            return totalWorkingHours;
        } catch (HibernateException e) {
            e.printStackTrace();
            if (trx != null) {
                trx.rollback();
            }
            return null;
        }

    }

}

