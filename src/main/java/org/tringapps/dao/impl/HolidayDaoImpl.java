package org.tringapps.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.tringapps.entities.EmployeeAttendance;
import org.tringapps.entities.Holidays;
import org.tringapps.util.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

@Repository
public class HolidayDaoImpl {

    public List<Long> getHolidaysList(long startDate, long endDate){
        Transaction transaction = null;
        int employeeId = -1;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            Criteria holidays = session.createCriteria(Holidays.class);
            holidays.add(Restrictions.between("date", startDate, endDate));
            List<Long> holidaysList = new ArrayList<>();
            List<Holidays> holidays1 = holidays.list();
            for(Holidays holiday : holidays1){
                holidaysList.add(holiday.getDate());
            }
            transaction.commit();
            return holidaysList;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            return null;
        }
    }
}
