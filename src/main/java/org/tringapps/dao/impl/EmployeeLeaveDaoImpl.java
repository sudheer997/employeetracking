package org.tringapps.dao.impl;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.tringapps.constants.SqlQueries;
import org.tringapps.entities.Employee;
import org.tringapps.entities.EmployeeLeaveDetails;
import org.tringapps.util.HibernateUtil;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


@Repository
public class EmployeeLeaveDaoImpl {

    public List<EmployeeLeaveDetails> getEmployeeLeaveRecords(Employee employee){

        Transaction trx = null;
        int id = 0;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            trx = session.beginTransaction();
            Query getQuey = session.createQuery(SqlQueries.getEmployeeLeaves);
            getQuey.setParameter("employee", employee);
            List<EmployeeLeaveDetails> employeeLeaveDetails = getQuey.list();
            session.getTransaction().commit();
            return employeeLeaveDetails;
        } catch (HibernateException e) {
            if (trx != null) {
                trx.rollback();
            }
            e.printStackTrace();
            return null;
        }
    }

    public int persistRecord(EmployeeLeaveDetails employeeLeaveDetails){
        Transaction trx = null;
        int id = 0;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            trx = session.beginTransaction();
            id = (Integer) session.save(employeeLeaveDetails);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            if (trx != null) {
                trx.rollback();
            }
            e.printStackTrace();
        }
        return id;
    }

    public Map<Integer, Integer> getEmployeeLeaveCount(long startDate, long endDate){
        if (startDate == -1 && endDate == -1) {
            return null;
        }
        Transaction trx = null;
        int id = 0;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            trx = session.beginTransaction();
            Query getQuey = session.createQuery(SqlQueries.getAllEmployeeLeaveCount);
            getQuey.setParameter("startDate", startDate);
            getQuey.setParameter("endDate", endDate);
            Map<Integer, Integer> employeeLeaveCount = new HashMap<>();
            Iterator<Object> iterator = getQuey.iterate();
            while (iterator.hasNext()){
                Object[] row = (Object[]) iterator.next();
                Employee employee = (Employee) row[0];
                Long noOfLeaves =  (Long)  row[1];
                System.out.println(noOfLeaves);
                int noOfLeaveCount = Integer.parseInt(String.valueOf(noOfLeaves));
                employeeLeaveCount.put(employee.getEmployeeId(), noOfLeaveCount);
            }
            /*List<List<Integer>> count = getQuey.list();
            for(List<Integer> record : count){
                employeeLeaveCount.put(record.get(0), record.get(1));
            }*/
            session.getTransaction().commit();
            return employeeLeaveCount;
        } catch (HibernateException e) {
            if (trx != null) {
                trx.rollback();
            }
            e.printStackTrace();
            return null;
        }
    }
}
