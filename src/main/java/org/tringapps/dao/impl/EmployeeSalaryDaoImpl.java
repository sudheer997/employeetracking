package org.tringapps.dao.impl;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.tringapps.entities.EmployeeAttendance;
import org.tringapps.entities.EmployeeSalaryDetails;
import org.tringapps.util.HibernateUtil;

import java.util.List;
import java.util.Map;

@Repository
public class EmployeeSalaryDaoImpl {
    public void persistEmployeeSalaryDetails(List<EmployeeSalaryDetails> employeeSalaryDetails){
        Transaction trx = null;
        if(employeeSalaryDetails == null){
            return;
        }
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            trx = session.beginTransaction();
            int item = 0;
            for(EmployeeSalaryDetails salaryDetails : employeeSalaryDetails){
                session.save(salaryDetails);
                ++item;
                if(item ==20){
                    session.flush();
                    session.clear();
                }
            }
            session.getTransaction().commit();
        } catch (HibernateException e) {
            if (trx != null) {
                trx.rollback();
            }
            e.printStackTrace();
        }
    }
}
