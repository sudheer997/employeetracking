package org.tringapps.dao.impl;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.tringapps.constants.SqlQueries;
import org.tringapps.util.HibernateUtil;

import java.util.List;

@Repository
public class LeaveDaoImpl {

    public int getCasualLeaveCount(){
        Transaction trx = null;
        int casualLeaveCount = 0;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            trx = session.beginTransaction();
            Query query = session.createQuery(SqlQueries.getLeaveCount);
            query.setParameter("leaveType", "casual");
            casualLeaveCount = query.executeUpdate();
            session.getTransaction().commit();
        } catch (HibernateException e) {
            if (trx != null) {
                trx.rollback();
            }
            e.printStackTrace();
//            return null;
        }
        return casualLeaveCount;
    }
}
