package org.tringapps.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.tringapps.constants.SqlQueries;
import org.tringapps.dao.EmployeeDao;
import org.tringapps.entities.Employee;
import org.tringapps.entities.Holidays;
import org.tringapps.util.HibernateUtil;


import java.util.List;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

    public int persistEmployee(Employee employee){
        Transaction transaction = null;
        int employeeId = -1;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            employeeId = (int) session.save(employee);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return employeeId;
    }

    public List fetchEmployee() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        List employee = null;
        int id = 0;
        try {
            transaction = session.beginTransaction();
            employee = session.createCriteria(Employee.class).list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return employee;
    }

    public Employee fetchEmployee(int employeeId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        Employee employee = null;
        int id = 0;
        try {
            transaction = session.beginTransaction();
            employee = session.get(Employee.class, employeeId);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return employee;
    }

    public List<Integer> getAllEmployeeIds(){
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            Query getQuery = session.createQuery(SqlQueries.getAllEmployeeIds);
            List<Integer> employeeIds = getQuery.list();
            transaction.commit();
            return employeeIds;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            return null;
        }
    }
}
