package org.tringapps.dao.impl;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.tringapps.constants.SqlQueries;
import org.tringapps.entities.Employee;
import org.tringapps.entities.EmployeeLeaveCount;
import org.tringapps.util.HibernateUtil;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Repository
public class EmployeeLeaveCountDaoImpl {

    public void persistEmployeeLeaveCount(EmployeeLeaveCount employeeLeaveCount){
        Transaction trx = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            trx = session.beginTransaction();
            session.save(employeeLeaveCount);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            if (trx != null) {
                trx.rollback();
            }
        }
    }

    public Map<Integer, Integer> getEmployeeLeaveCount(){
        Transaction trx = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            trx = session.beginTransaction();
            Query updateQuery = session.createQuery(SqlQueries.getCasualLeaveCount);
            Iterator iterator = updateQuery.iterate();
            Map<Integer, Integer> casualLeaveCount = new HashMap<>();
            while (iterator.hasNext()){
                Object[] row = (Object[]) iterator.next();
                Employee employee = (Employee) row[0];
                int casualLeaves = (int) row[1];
                casualLeaveCount.put(employee.getEmployeeId(), casualLeaves);

            }
            session.getTransaction().commit();
            return casualLeaveCount;
        } catch (HibernateException e) {
            e.printStackTrace();
            if (trx != null) {
                trx.rollback();
            }
            return null;
        }

    }
}
