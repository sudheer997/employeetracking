package org.tringapps.dao;

import org.tringapps.entities.EmployeeAttendance;

public interface EmployeeAttendanceDao {
    int persistRecord(EmployeeAttendance employeeAttendance);
    int updateCheckOutTime(int checkOutTime, int employeeId, long date);
    int deleteRecord(int employeeId, long date);

}
