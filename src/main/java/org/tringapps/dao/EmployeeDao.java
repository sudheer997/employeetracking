package org.tringapps.dao;


import org.tringapps.entities.Employee;

import java.util.List;

public interface EmployeeDao {
    Employee fetchEmployee(int employeeId);

    List<Integer> getAllEmployeeIds();

}
