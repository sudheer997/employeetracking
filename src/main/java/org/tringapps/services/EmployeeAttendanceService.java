package org.tringapps.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tringapps.dao.impl.EmployeeAttendanceDaoImpl;
import org.tringapps.dao.impl.EmployeeDaoImpl;
import org.tringapps.dao.impl.HolidayDaoImpl;
import org.tringapps.entities.Employee;
import org.tringapps.entities.EmployeeAttendance;
import org.tringapps.util.DateUtil;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

@Service
public class EmployeeAttendanceService {
    @Autowired
    EmployeeAttendanceDaoImpl employeeAttendanceDao;
    @Autowired
    EmployeeDaoImpl employeeDao;
    @Autowired
    HolidayDaoImpl holidayDao;
    @Autowired
    EmployeeLeaveService employeeLeaveService;

    public List<String> createEmployeeAttendanceRecords(InputStream file) throws IOException {
        CSVReader employeeAttendanceRecords = new CSVReaderBuilder(new InputStreamReader(file)).
                withSkipLines(1).build();
        List<String[]> records = employeeAttendanceRecords.readAll();
        Map<Integer, List<EmployeeAttendance>> validAttendanceRecords = new
                HashMap<Integer, List<EmployeeAttendance>>();
        List<String> inValidRecords = new ArrayList<String>();
        for (String[] record : records) {
            String employeeId = record[0];
            try {
                int employee_id = Integer.parseInt(employeeId.trim());
                Employee employee = employeeDao.fetchEmployee(employee_id);
                if(employee == null){
                    inValidRecords.add(employeeId);
                    continue;
                }
                long checkInTime = DateUtil.timeToMilliSecs(record[1].trim());
                if(checkInTime == -1){
                    inValidRecords.add(employeeId);
                    continue;
                }
                long checkOutTime = DateUtil.timeToMilliSecs(record[2].trim());
                if(checkOutTime == -1 || checkOutTime < checkInTime){
                    inValidRecords.add(employeeId);
                    continue;
                }
                long date = DateUtil.dateToTimeStamp(record[3].trim(), "dd/MM/yyyy");
                if(date == -1 || validateDateInPreviousAttendanceRecords(employee.getEmployeeAttendances(), date) || 
                		validateDateInPreviousAttendanceRecords(validAttendanceRecords.getOrDefault(employee_id, new ArrayList<EmployeeAttendance>()), date)){
                    inValidRecords.add(employeeId);
                    continue;
                }
                EmployeeAttendance employeeAttendance = new EmployeeAttendance();
                employeeAttendance.setEmployee(employee);
                employeeAttendance.setDate(date);
                employeeAttendance.setCheckInTime(checkInTime);
                employeeAttendance.setCheckOutTime(checkOutTime);
                employeeAttendance.setTotalHours(checkOutTime - checkInTime);
                List<EmployeeAttendance> employeeAttendances = validAttendanceRecords.getOrDefault(employee_id,
                        new ArrayList<>());
                employeeAttendances.add(employeeAttendance);
                validAttendanceRecords.put(employee_id, employeeAttendances);

            } catch (Exception e) {
            	e.printStackTrace();
                inValidRecords.add(employeeId);
            }
        }
        employeeAttendanceDao.persistMultipleRecords(validAttendanceRecords);
        return inValidRecords;
    }

    public boolean validateDateInPreviousAttendanceRecords(List<EmployeeAttendance> employeeAttendances,
                                                           long date){
        if(employeeAttendances == null){
            return false;
        }
        for(EmployeeAttendance employeeAttendance : employeeAttendances){
            if(employeeAttendance.getDate() == date){
                return true;
            }
        }
        return false;
    }

    public Map<Integer, String> getWorkingHours() throws ParseException {
        /*if(DateUtil.getCurrentDay() != 5){
            return null;
        }*/
        Long[] startAndEndDate = DateUtil.getStartDateAndEndDate();
        if(startAndEndDate == null){
            return null;
        }
        long startDate = startAndEndDate[0];
        long endDate = startAndEndDate[1];
        List<EmployeeAttendance> employeeWeeklyAttendanceRecords= employeeAttendanceDao.
                getEmployeeWeeklyAttendanceRecords(startDate, endDate);
        List<Long> holidaysList = holidayDao.getHolidaysList(startDate, endDate);
        List<Integer> employeeIds = employeeDao.getAllEmployeeIds();
        List<Long> workingDays = DateUtil.getListDays(startDate, endDate);
        workingDays.removeAll(holidaysList);

        Map<Integer, String> totalWorkingHours = new HashMap<>();
        for(int employeeId : employeeIds){
            List<EmployeeAttendance> records = new ArrayList<>();
            List<Long> dates = new ArrayList<>();
            long employeeWorkingHours = 0L;
            for(EmployeeAttendance employeeAttendance : employeeWeeklyAttendanceRecords){
                if(employeeAttendance.getEmployee().getEmployeeId() == employeeId){
                    employeeWorkingHours += employeeAttendance.getTotalHours();
                    System.out.println(employeeWorkingHours);
                    records.add(employeeAttendance);
                    dates.add(employeeAttendance.getDate());
                }
            }
            totalWorkingHours.put(employeeId, DateUtil.milliSecondsToTime(employeeWorkingHours));
            if(records.size() != 5 - holidaysList.size()){
                workingDays.removeAll(dates);
                if(workingDays.size() != 0){
                    employeeLeaveService.createLeave(workingDays, employeeId);
                }
            }
        }
        return totalWorkingHours;
    }

}

