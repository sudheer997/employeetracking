package org.tringapps.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tringapps.dao.EmployeeDao;
import org.tringapps.dao.impl.EmployeeLeaveDaoImpl;
import org.tringapps.entities.Employee;
import org.tringapps.entities.EmployeeLeaveDetails;

import java.util.List;

import javax.transaction.Transactional;

@Service
public class EmployeeLeaveService {
    @Autowired
    EmployeeDao employeeDao;
    @Autowired
    EmployeeLeaveDaoImpl employeeLeaveDao;
    
    @Transactional
    public void createLeave(List<Long> dates, int employeeId){
        Employee employee = employeeDao.fetchEmployee(employeeId);
        List<EmployeeLeaveDetails> employeeLeaveDetail = employeeLeaveDao.getEmployeeLeaveRecords(employee);
        System.out.println("employee reco");
        for(Long date: dates){
            if(validateDateInPreviousLeaveDetails(employeeLeaveDetail, date)){
                continue;
            }
//            List<EmployeeLeaveDetails> employeeLeaveDetails = employee.getEmployeeLeaveDetails();
            EmployeeLeaveDetails employeeLeaveDetails = new EmployeeLeaveDetails();
            employeeLeaveDetails.setDate(date);
            employeeLeaveDetails.setEmployee(employee);
            employeeLeaveDao.persistRecord(employeeLeaveDetails);
        }

    }
    
    @Transactional
    public boolean validateDateInPreviousLeaveDetails(List<EmployeeLeaveDetails> employeeLeaveDetails, long date){
        if(employeeLeaveDetails == null){
            return false;
        }
        for(EmployeeLeaveDetails leaveDetails : employeeLeaveDetails){
            if(leaveDetails.getDate() == date){
                return true;
            }
        }
        return false;
    }
}
