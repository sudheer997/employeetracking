package org.tringapps.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tringapps.dao.impl.EmployeeDaoImpl;
import org.tringapps.dao.impl.EmployeeLeaveCountDaoImpl;
import org.tringapps.entities.Employee;
import org.tringapps.entities.EmployeeLeaveCount;
import org.tringapps.entities.PersonalDetails;

@Service
public class EmployeeService {
    @Autowired
    EmployeeDaoImpl employeeDao;
    @Autowired
    EmployeeLeaveCountDaoImpl employeeLeaveCountDao;

    public void createDefaultEmployee(){
        Employee employee = new Employee();
        employee.setEmailId("test@gmail.com");
        employee.setRole("developer");
        employee.setBasicSalary(10000);
        int employeeId = employeeDao.persistEmployee(employee);

        Employee employee1 = new Employee();
        employee1.setEmailId("test2@gmail.com");
        employee1.setRole("developer");
        employee1.setBasicSalary(12000);
        employeeDao.persistEmployee(employee1);

        Employee employee2 = new Employee();
        employee2.setEmailId("test3@gmail.com");
        employee2.setRole("HR");
        employee2.setBasicSalary(15000);
        employeeDao.persistEmployee(employee2);

        EmployeeLeaveCount employeeLeaveCount = new EmployeeLeaveCount();
        employeeLeaveCount.setEmployee(employee);
        employeeLeaveCount.setCasualLeaveCount(6);
        employeeLeaveCountDao.persistEmployeeLeaveCount(employeeLeaveCount);

        EmployeeLeaveCount employeeLeaveCount2 = new EmployeeLeaveCount();
        employeeLeaveCount2.setEmployee(employee1);
        employeeLeaveCount2.setCasualLeaveCount(6);
        employeeLeaveCountDao.persistEmployeeLeaveCount(employeeLeaveCount2);


        EmployeeLeaveCount employeeLeaveCount3 = new EmployeeLeaveCount();
        employeeLeaveCount3.setEmployee(employee2);
        employeeLeaveCount3.setCasualLeaveCount(6);
        employeeLeaveCountDao.persistEmployeeLeaveCount(employeeLeaveCount3);


    }
}
