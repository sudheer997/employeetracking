package org.tringapps.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tringapps.dao.EmployeeDao;
import org.tringapps.dao.impl.*;
import org.tringapps.entities.Employee;
import org.tringapps.entities.EmployeeSalaryDetails;
import org.tringapps.util.DateUtil;

import java.util.*;

@Service
public class EmployeeSalaryService {
    @Autowired
    EmployeeAttendanceDaoImpl employeeAttendanceDao;
    @Autowired
    HolidayDaoImpl holidayDao;
    @Autowired
    EmployeeLeaveDaoImpl employeeLeaveDao;
    @Autowired
    EmployeeDao employeeDao;
    @Autowired
    LeaveDaoImpl leaveDao;
    @Autowired
    EmployeeLeaveCountDaoImpl employeeLeaveCountDao;
    @Autowired
    EmployeeSalaryDaoImpl employeeSalaryDao;

    public List<EmployeeSalaryDetails> getSalaryDetails(){
        Long[] startAndEndDate = DateUtil.getMonthStartAndEndDate();
        if(startAndEndDate == null){
            return null;
        }
        long startDate = startAndEndDate[0];
        long endDate = startAndEndDate[1];
        int totalWorkingDays = DateUtil.totalWorkingDays(startDate, endDate);
        List<Long> holidaysList = holidayDao.getHolidaysList(startDate, endDate);
        if(holidaysList != null){
            totalWorkingDays -= holidaysList.size();
        }
        long actualWorkingHours = DateUtil.getActualHours(totalWorkingDays);
        Map<Integer, Long> totalWorkingHours = employeeAttendanceDao.getTotalWorkingHours(startDate, endDate);
        System.out.println("total working hours" + totalWorkingHours);
        Map<Integer, Integer> currentMonthEmployeeLeaveCount = employeeLeaveDao.getEmployeeLeaveCount(startDate, endDate);
        Map<Integer, Integer> availableEmployeeLeaveCount = employeeLeaveCountDao.getEmployeeLeaveCount();
        System.out.println("Avaliable leave count" + availableEmployeeLeaveCount);
        Map<Integer, List<Integer>> paidUnpaidLeaves = getPaidAndUnPaidLeaves(currentMonthEmployeeLeaveCount,
                availableEmployeeLeaveCount);
        List<Integer> employeeIds = employeeDao.getAllEmployeeIds();
        List<EmployeeSalaryDetails> employeeSalaryDetails = new ArrayList<>();

        for(int employeeId : employeeIds){
            EmployeeSalaryDetails employeeSalaryDetail = new EmployeeSalaryDetails();
            Employee employee = employeeDao.fetchEmployee(employeeId);
            employeeSalaryDetail.setEmployee(employee);
            employeeSalaryDetail.setYear(DateUtil.getPreviousMonthYear());
            employeeSalaryDetail.setMonth(DateUtil.getPreviousMonth());
            employeeSalaryDetail.setTotalHours(totalWorkingHours.get(employeeId));
            if(holidaysList != null){
                employeeSalaryDetail.setHolidays(holidaysList.size());
            }
            else{
                employeeSalaryDetail.setHolidays(0);
            }
            if(paidUnpaidLeaves.get(employeeId) != null){
                employeeSalaryDetail.setPaidLeaves(paidUnpaidLeaves.get(employeeId).get(0));
                employeeSalaryDetail.setUnPaidLeaves(paidUnpaidLeaves.get(employeeId).get(1));
            }
            else{
                employeeSalaryDetail.setPaidLeaves(0);
                employeeSalaryDetail.setUnPaidLeaves(0);
            }
            int salary = calculateSalary(totalWorkingHours.get(employeeId), actualWorkingHours,
                    currentMonthEmployeeLeaveCount.getOrDefault(employeeId, 0),
                    employee.getBasicSalary(), employeeSalaryDetail.getUnPaidLeaves());
            employeeSalaryDetail.setSalaryId(salary);
            employeeSalaryDetails.add(employeeSalaryDetail);
        }
        employeeSalaryDao.persistEmployeeSalaryDetails(employeeSalaryDetails);
        for(EmployeeSalaryDetails employeeSalaryDetails1 : employeeSalaryDetails){
            System.out.println(employeeSalaryDetails1);
        }
        return employeeSalaryDetails;
    }

    public Map<Integer, List<Integer>> getPaidAndUnPaidLeaves(Map<Integer, Integer> currentMonthEmployeeLeaveCount,
                                       Map<Integer, Integer> availableEmployeeLeaveCount){
        if(currentMonthEmployeeLeaveCount == null || availableEmployeeLeaveCount == null){
            return null;
        }
        Map<Integer, List<Integer>> paidAndUnPaidLeaves = new HashMap<>();
        for(Map.Entry<Integer, Integer> record : currentMonthEmployeeLeaveCount.entrySet()){
            List<Integer> leaveCount = new ArrayList<>();
            int availableLeaveCount = availableEmployeeLeaveCount.get(record.getKey());
            int takenLeaveCount = record.getValue();
            int currentMonth = DateUtil.getPreviousMonthYear()  ;
            int availableMonthCount= availableLeaveCount - (12- currentMonth) + 1;
            leaveCount.add(availableLeaveCount);
            int unPaidLeaves = takenLeaveCount -availableMonthCount;
            leaveCount.add(unPaidLeaves);
            paidAndUnPaidLeaves.put(record.getKey(), leaveCount);
        }
        return paidAndUnPaidLeaves;
    }

    public int calculateSalary(long totalWorkingHours, long actualWorkingHours, int takenLeaves,
                                int basicSalary, int unPaidLeaves){
        totalWorkingHours += DateUtil.getActualHours(takenLeaves);
        long remainingHours = actualWorkingHours - totalWorkingHours;
        int days = (int) (remainingHours/DateUtil.getActualHours(1));
        int oneDaySalary = basicSalary/30;
        int monthSalary = basicSalary - days * oneDaySalary - unPaidLeaves * oneDaySalary;
        System.out.println("salary : " + monthSalary);
        return monthSalary;
    }
}
