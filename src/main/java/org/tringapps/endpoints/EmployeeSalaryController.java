package org.tringapps.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tringapps.entities.EmployeeSalaryDetails;
import org.tringapps.services.EmployeeSalaryService;

import java.util.List;

@RestController
public class EmployeeSalaryController {
    @Autowired
    EmployeeSalaryService employeeSalaryService;

    @GetMapping(value = "/salaryDetails")
    public String getSalaryDetails(){

        employeeSalaryService.getSalaryDetails();
        return "sample";
    }
}
