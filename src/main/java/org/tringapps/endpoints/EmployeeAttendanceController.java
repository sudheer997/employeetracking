package org.tringapps.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tringapps.services.EmployeeAttendanceService;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Map;

@RestController
public class EmployeeAttendanceController {
    @Autowired
    EmployeeAttendanceService employeeAttendanceService;

    @GetMapping(value = "/getWorkingHours")
    public  Map<Integer, String> getWorkingHours(){
        Map<Integer, String> totalWorkingHours = null;
        try {
            totalWorkingHours  = employeeAttendanceService.getWorkingHours();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return totalWorkingHours;
    }
}
