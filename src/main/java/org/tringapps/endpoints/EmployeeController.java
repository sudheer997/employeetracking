package org.tringapps.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tringapps.services.EmployeeService;

@RestController()
public class EmployeeController {
    @Autowired
    EmployeeService employeeService;

    @GetMapping("/create")
    public String createEmployee(){
        employeeService.createDefaultEmployee();
        return "okay";
        }
}
