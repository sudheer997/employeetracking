package org.tringapps.endpoints;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.tringapps.services.EmployeeAttendanceService;

@RestController
public class FileUpload {
    @Autowired
    EmployeeAttendanceService employeeAttendanceService;

    @PostMapping("/uploadFile")
    public String uploadFile(@RequestPart(value = "file") MultipartFile file){
        String filename=file.getOriginalFilename();
        if(!(filename != null && filename.endsWith(".csv"))){
            return "Invalid File";
        }
        try{
            List<String> invalidEmployeeRecords = employeeAttendanceService.
                    createEmployeeAttendanceRecords(file.getInputStream());
        }
        catch(Exception e){System.out.println(e);}
        
        return "File upload Successfully";

    }
}
