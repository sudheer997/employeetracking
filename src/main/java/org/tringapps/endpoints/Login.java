package org.tringapps.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.tringapps.services.LoginService;


@RestController
public class Login {
    @Autowired
    LoginService loginService;

    @GetMapping("/login")
    public String login(@RequestParam("userName") String userName, @RequestParam("password") String password){
        boolean loginStatus = loginService.validCredentials(userName, password);

        if(!loginStatus){
//            Invalid user
        }
        return "hello";
    }
}
