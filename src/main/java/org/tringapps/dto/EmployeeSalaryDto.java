package org.tringapps.dto;

import org.springframework.stereotype.Component;

@Component
public class EmployeeSalaryDto {
    private int EmployeeId;
    private String workingHours;
    private int unPaidLeaves;
    private int paidLeaves;
    private int salaryAmount;

    public int getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(int employeeId) {
        EmployeeId = employeeId;
    }

    public String getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(String workingHours) {
        this.workingHours = workingHours;
    }

    public int getUnPaidLeaves() {
        return unPaidLeaves;
    }

    public void setUnPaidLeaves(int unPaidLeaves) {
        this.unPaidLeaves = unPaidLeaves;
    }

    public int getPaidLeaves() {
        return paidLeaves;
    }

    public void setPaidLeaves(int paidLeaves) {
        this.paidLeaves = paidLeaves;
    }

    public int getSalaryAmount() {
        return salaryAmount;
    }

    public void setSalaryAmount(int salaryAmount) {
        this.salaryAmount = salaryAmount;
    }
}
