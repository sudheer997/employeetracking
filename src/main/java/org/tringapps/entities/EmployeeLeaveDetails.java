package org.tringapps.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "employee_leave_details")
public class EmployeeLeaveDetails implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "employee_leave_id")
    private int employeeLeaveId;

    @Column(name = "date")
    private long date;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;

    @Embedded
    private AuditField auditField;

    public EmployeeLeaveDetails() {
        super();
    }

    public EmployeeLeaveDetails(int employeeLeaveId, int employeeId, long date, AuditField auditField) {
        super();
        this.employeeLeaveId = employeeLeaveId;
        this.date = date;
        this.auditField = auditField;
    }


    public int getEmployeeLeaveId() {
        return employeeLeaveId;
    }

    public void setEmployeeLeaveId(int employeeLeaveId) {
        this.employeeLeaveId = employeeLeaveId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public AuditField getAuditField() {
        return auditField;
    }

    public void setAuditField(AuditField auditField) {
        this.auditField = auditField;
    }

    @Override
    public String toString() {
        return "EmployeeLeaveDetails{" +
                "employeeLeaveId=" + employeeLeaveId +
                ", date=" + date +
                ", employee=" + employee +
                '}';
    }
}
