package org.tringapps.entities;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "employee_personal_details")
public class PersonalDetails implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "personal_details_id")
    private int personal_details_id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "employee_id")
    private Employee employee;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "gender")
    private String gender;

    @Embedded
    private AuditField auditField;

    public int getPersonal_details_id() {
        return personal_details_id;
    }

    public void setPersonal_details_id(int personal_details_id) {
        this.personal_details_id = personal_details_id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public AuditField getAuditField() {
        return auditField;
    }

    public void setAuditField(AuditField auditField) {
        this.auditField = auditField;
    }


}
