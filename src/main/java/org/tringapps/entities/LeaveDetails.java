package org.tringapps.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "leave_details")
public class LeaveDetails implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "leave_id")
    private int leaveId;

    @Column(name = "type")
    private String type;

    @Column(name = "count")
    private int count;

    @Embedded
    private AuditField auditField;

    public LeaveDetails() {
        super();
    }

    public LeaveDetails(int leaveId, String type, int count) {
        super();
        this.leaveId = leaveId;
        this.type = type;
        this.count = count;
    }

    public int getLeaveId() {
        return leaveId;
    }

    public void setLeaveId(int leaveId) {
        this.leaveId = leaveId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public AuditField getAuditField() {
        return auditField;
    }

    public void setAuditField(AuditField auditField) {
        this.auditField = auditField;
    }
}
