package org.tringapps.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "employee_leave_count")
public class EmployeeLeaveCount implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "leave_count_id")
    private int leaveCountId;

    @Column(name = "casual_leave_count")
    private int casualLeaveCount;

    @Column(name = "vacation_leave_count")
    private int vacationLeaveCount;

    @Embedded
    private AuditField auditField;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "employee_id")
    private Employee employee;


    public EmployeeLeaveCount() {
        super();
    }

    public EmployeeLeaveCount(int leaveCountId, int employeeId, int casualLeaveCount) {
        super();
        this.leaveCountId = leaveCountId;
        this.casualLeaveCount = casualLeaveCount;
    }

    public int getLeaveCountId() {
        return leaveCountId;
    }

    public void setLeaveCountId(int leaveCountId) {
        this.leaveCountId = leaveCountId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public int getCasualLeaveCount() {
        return casualLeaveCount;
    }

    public void setCasualLeaveCount(int casualLeaveCount) {
        this.casualLeaveCount = casualLeaveCount;
    }


    public int getVacationLeaveCount() {
        return vacationLeaveCount;
    }

    public void setVacationLeaveCount(int vacationLeaveCount) {
        this.vacationLeaveCount = vacationLeaveCount;
    }

    public AuditField getAuditField() {
        return auditField;
    }

    public void setAuditField(AuditField auditField) {
        this.auditField = auditField;
    }


}
