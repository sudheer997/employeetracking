package org.tringapps.entities;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "employee")
@SequenceGenerator(name = "employee", initialValue = 1000)
public class Employee implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "employee_id")
    private int employeeId;

    @Column(name = "email_id")
    private String emailId;

    @Column(name = "role")
    private String role;

    @Column(name = "basic_salary")
    private int basicSalary;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @Embedded
    private AuditField auditField;

    @OneToMany(mappedBy = "employee", fetch=FetchType.EAGER)
    private List<EmployeeAttendance> employeeAttendances;

    @OneToMany(mappedBy = "employee", fetch=FetchType.LAZY)
    private List<EmployeeLeaveDetails> employeeLeaveDetails;

    @OneToMany(mappedBy = "employee")
    private List<EmployeeSalaryDetails> employeeSalaryDetails;


    public Employee(int employeeId, String emailId, String role, int basicSalary, AuditField auditField) {
        super();
        this.employeeId = employeeId;
        this.emailId = emailId;
        this.role = role;
        this.basicSalary = basicSalary;
        this.auditField = auditField;
    }

    public Employee() {
        super();
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getBasicSalary() {
        return basicSalary;
    }

    public void setBasicSalary(int basicSalary) {
        this.basicSalary = basicSalary;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public AuditField getAuditField() {
        return auditField;
    }

    public void setAuditField(AuditField auditField) {
        this.auditField = auditField;
    }

    public List<EmployeeAttendance> getEmployeeAttendances() {
        return employeeAttendances;
    }

    public void setEmployeeAttendances(List<EmployeeAttendance> employeeAttendances) {
        this.employeeAttendances = employeeAttendances;
    }

    public List<EmployeeLeaveDetails> getEmployeeLeaveDetails() {
        return employeeLeaveDetails;
    }

    public void setEmployeeLeaveDetails(List<EmployeeLeaveDetails> employeeLeaveDetails) {
        this.employeeLeaveDetails = employeeLeaveDetails;
    }

    public List<EmployeeSalaryDetails> getEmployeeSalaryDetails() {
        return employeeSalaryDetails;
    }

    public void setEmployeeSalaryDetails(List<EmployeeSalaryDetails> employeeSalaryDetails) {
        this.employeeSalaryDetails = employeeSalaryDetails;
    }
}