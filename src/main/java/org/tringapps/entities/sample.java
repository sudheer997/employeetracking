package org.tringapps.entities;
import org.hibernate.Session;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tringapps.util.HibernateUtil;

@RestController
public class sample {

    @GetMapping(value = "/hello")
    public String sample1(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        return "sample";
    }

}
