package org.tringapps.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "holidays")
public class Holidays implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "holiday_id")
    private int holidayId;

    @Column(name = "name")
    private String name;

    @Column(name = "date")
    private long date;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @Embedded
    private AuditField auditField;

    public Holidays() {
        super();
    }

    public Holidays(int holidayId, String name, long date, AuditField auditField){
        this.holidayId = holidayId;
        this.name = name;
        this.date = date;
        this.auditField = auditField;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHolidayId() {
        return holidayId;
    }

    public void setHolidayId(int holidayId) {
        this.holidayId = holidayId;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public AuditField getAuditField() {
        return auditField;
    }

    public void setAuditField(AuditField auditField) {
        this.auditField = auditField;
    }
}
