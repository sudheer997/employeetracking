package org.tringapps.entities;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "employee_attendance")
public class EmployeeAttendance implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "attendance_id")
    private int attendanceID;
    @Column(name = "date")
    private long date;
    @Column(name = "checkin_time")
    private long checkInTime;
    @Column(name = "checkout_time")
    private long checkOutTime;
    @Column(name = "total_hours")
    private long totalHours;
    @Column(name = "is_deleted")
    private boolean isDeleted;
    @Embedded
    private AuditField auditField;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;

    public EmployeeAttendance(int attendanceID, long date, long checkInTime, long checkOutTime,
                              int totalHours, AuditField auditField) {
        super();
        this.attendanceID = attendanceID;
        this.date = date;
        this.checkInTime = checkInTime;
        this.checkOutTime = checkOutTime;
        this.totalHours = totalHours;
        this.auditField = auditField;
    }

    public EmployeeAttendance() {
        super();
    }

    public int getAttendanceID() {
        return attendanceID;
    }

    public void setAttendanceID(int attendanceID) {
        this.attendanceID = attendanceID;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public long getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(long checkInTime) {
        this.checkInTime = checkInTime;
    }

    public long getCheckOutTime() {
        return checkOutTime;
    }

    public void setCheckOutTime(long checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    public long getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(long totalHours) {
        this.totalHours = totalHours;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public AuditField getAuditField() {
        return auditField;
    }

    public void setAuditField(AuditField auditField) {
        this.auditField = auditField;
    }


}
