package org.tringapps.entities;

import javax.persistence.*;

@Entity
@Table(name = "employee_salary_details")
public class EmployeeSalaryDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "salary_id")
    private int salaryId;
    @Column(name = "year")
    private int year;
    @Column(name = "month")
    private int month;
    @Column(name = "total_hours")
    private long totalHours;
    @Column(name = "paid_leaves")
    private int paidLeaves;
    @Column(name = "unpaid_leaves")
    private int unPaidLeaves;
    @Column(name = "holidays")
    private int holidays;
    @Column(name = "amount")
    private int amount;
    @Embedded
    private AuditField auditField;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;

	public int getSalaryId() {
		return salaryId;
	}

	public void setSalaryId(int salaryId) {
		this.salaryId = salaryId;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public long getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(long totalHours) {
		this.totalHours = totalHours;
	}

	public int getPaidLeaves() {
		return paidLeaves;
	}

	public void setPaidLeaves(int paidLeaves) {
		this.paidLeaves = paidLeaves;
	}

	public int getUnPaidLeaves() {
		return unPaidLeaves;
	}

	public void setUnPaidLeaves(int unPaidLeaves) {
		this.unPaidLeaves = unPaidLeaves;
	}

	public int getHolidays() {
		return holidays;
	}

	public void setHolidays(int holidays) {
		this.holidays = holidays;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public AuditField getAuditField() {
		return auditField;
	}

	public void setAuditField(AuditField auditField) {
		this.auditField = auditField;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
    
    
    

}
